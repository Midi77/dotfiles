" Automatic reloading of .vimrc
autocmd! bufwritepost .vimrc source %
autocmd! bufwritepost :SyntasticCheck

" Better copy & paste
set pastetoggle=<F2>
set clipboard=unnamed


" Mouse and backspace
set mouse=a  " on OSX press ALT and click
set bs=2     " make backspace behave like normal again
set cursorline
set showmatch
set backspace=indent,eol,start              " backspace removes all (indents, EOLs, start) What is start?


" Rebind <Leader> key
let mapleader = ","


" Bind nohl
" Removes highlight of your last search
" ``<C>`` stands for ``CTRL`` and therefore ``<C-n>`` stands for ``CTRL+n``
noremap <C-n> :nohl<CR>
vnoremap <C-n> :nohl<CR>
inoremap <C-n> :nohl<CR>


" Quicksave command
noremap <C-Z> :update<CR>
vnoremap <C-Z> <C-C>:update<CR>
inoremap <C-Z> <C-O>:update<CR>


" Quick quit command
noremap <Leader>e :quit<CR>  " Quit current window
noremap <Leader>E :qa!<CR>   " Quit all windows


" bind Ctrl+<movement> keys to move around the windows, instead of using Ctrl+w + <movement>
" Every unnecessary keystroke that can be saved is good for your health :)
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" easier moving between tabs
map <Leader>n <esc>:tabprevious<CR>
map <Leader>; <esc>:tabnext<CR>


" map sort function to a key, sorting of imports
vnoremap <Leader>s :sort<CR>


" easier moving of code blocks
" Try to go into visual mode (v), thenselect several lines of code here and
" then press ``>`` several times.
vnoremap < <gv  " better indentation
vnoremap > >gv  " better indentation


" Show whitespace
" MUST be inserted BEFORE the colorscheme command
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
au InsertLeave * match ExtraWhitespace /\s\+$/


" Enable syntax highlighting
" You need to reload this file for the change to apply
filetype plugin indent on
syntax on


" Showing line numbers and length
set number  " show line numbers
set tw=79   " width of document (used by gd)
set nowrap  " don't automatically wrap on load
set fo-=t   " don't automatically wrap text when typing
set colorcolumn=80
highlight ColorColumn ctermbg=233


" easier formatting of paragraphs
vmap Q gq
nmap Q gqap


" Real programmers don't use TABs but spaces
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab


" Make search case insensitive
set hlsearch
set incsearch
set ignorecase
set smartcase


" Disable stupid backup and swap files - they trigger too many events
" for file system watchers
set nobackup
set nowritebackup
set noswapfile


" ============================================================================
" Python IDE Setup with Vundle
" ============================================================================

set nocompatible              		" be iMproved, required for Vundle
filetype off                  		" required for Vundle
set rtp+=~/.vim/bundle/Vundle.vim	" required for Vundle

call vundle#begin()					" required for Vundle
" Here come the mighty plugins


Plugin 'scrooloose/nerdtree'                " Project and file navigation
Plugin 'flazz/vim-colorschemes'             " Colorschemes
Plugin 'majutsushi/tagbar'                  " Class/module browser
Plugin 'davidhalter/jedi-vim'               " Jedi-vim autocomplete plugin
Plugin 'scrooloose/syntastic'               " Syntastic for checkers to avoid
                                            " Python mode
Plugin 'valloric/youcompleteme'
"INSTALLATION
"sudo apt-get install build-essential cmake
"sudo apt-get install python-dev
"cd ~/.vim/bundle/youcompleteme
"./install.py --clang-completer

Plugin 'bling/vim-airline'                  " Lean & mean status/tabline for vim
Plugin 'Lokaltog/powerline'                 " Powerline fonts plugin
Plugin 'tpope/vim-commentary'
Plugin 'hynek/vim-python-pep8-indent'
call vundle#end()           		        " required for Vundle
filetype plugin indent on   		        " required for Vundle


" My settings for Nerdtree
map <C-o> :NERDTreeToggle<CR>

" My settings for TagBar (requires sudo apt-get install exuberant-ctags)
"nmap <F8> :TagbarToggle<CR>
map <Leader>c :TagbarToggle<CR>

" Settings for vim-powerline
set laststatus=2


" Settings for jedi-vim (Martin)
let g:jedi#usages_command = "<leader>z"
let g:jedi#popup_on_dot = 1
let g:jedi#popup_select_first = 1
map <Leader>b Oimport ipdb; ipdb.set_trace() # BREAKPOINT<C-c>

" Setting for Syntastic  (pip install pep8, pylint, pyflakes etc.)
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1
let g:syntastic_aggregate_errors = 1
let g:syntastic_python_checkers = ['pep8', 'pyflakes', 'pylint', 'python']

" Color Scheme
set t_Co=256
color wombat256
autocmd ColorScheme * highlight Normal ctermbg=None
autocmd ColorScheme * highlight NonText ctermbg=None



" MOST IMPORTANT KEY COMMANDS
"
"" JEDI VIM

" select + shift K for Documentation
" Completion <C-Space>
" Goto assignments <leader>g (typical goto function)
" Goto definitions <leader>d (follow identifier as far as possible, includes imports and statements)
" Show Documentation/Pydoc K (shows a popup with assignments)
" Renaming <leader>r
" Usages <leader>n doesn't work in my setting, use CTRL Z,n
" Open module, e.g. :Pyimport os (opens the os module)
" 
" TAGBAR
" leader c open it up
" * open all folds
" = close all folds
" insde the window "p"
" space

" VIM COMMENTARY
" select a block and typ 'gc'

" NERD tree (5.0.0) quickhelp~
" ============================
" File node mappings~
" double-click,
" <CR>,
" o: open in prev window
" go: preview
" t: open in new tab
" T: open in new tab silently
" middle-click,
" i: open split
" gi: preview split
" s: open vsplit
" gs: preview vsplit
"
" ----------------------------
" Directory node mappings~
" double-click,
" o: open & close node
" O: recursively open node
" x: close parent of node
" X: close all child nodes of
"    current node recursively
" middle-click,
" e: explore selected dir
"
" ----------------------------
" Bookmark table mappings~
" double-click,
" o: open bookmark
" t: open in new tab
" T: open in new tab silently
" D: delete bookmark
"
" ----------------------------
" Tree navigation mappings~
" P: go to root
" p: go to parent
" K: go to first child
" J: go to last child
" <C-j>: go to next sibling
" <C-k>: go to prev sibling
"
" ----------------------------
" Filesystem mappings~
" C: change tree root to the
"    selected dir
" u: move tree root up a dir
" U: move tree root up a dir
"    but leave old root open
" r: refresh cursor dir
" R: refresh current root
" m: Show menu
" cd:change the CWD to the
"    selected dir
" CD:change tree root to CWD
"
" ----------------------------
" Tree filtering mappings~
" I: hidden files (off)
" f: file filters (on)
" F: files (on)
" B: bookmarks (off)
"
" ----------------------------
" Custom mappings~
"
" ----------------------------
" Other mappings~
" q: Close the NERDTree window
" A: Zoom (maximize-minimize)
"    the NERDTree window
" ?: toggle help
"
" ----------------------------
" Bookmark commands~
" :Bookmark [<name>]
" :BookmarkToRoot <name>
" :RevealBookmark <name>
" :OpenBookmark <name>
" :ClearBookmarks [<names>]
" :ClearAllBookmarks





